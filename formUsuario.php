<?php 
	include 'clases/Usuario.php';
	$users = new Usuario();
	$list = $users->listarUsuario();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Prueba Medoo</title>
	<link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12" style="background-color: #8FEBD6;">
				<div class="form-group" style="padding-top: 40px;">
					<label for="input" class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<input type="text" name="nombre" id="nombre" class="form-control" value="" required="required" pattern="" title="">
					</div>
				</div>
		
				<div class="form-group"  style="padding: 40px 0;">
					<label for="input" class="col-sm-2 control-label">Genero:</label>
					<div class="col-sm-10">
						<input type="text" name="genero" id="genero" class="form-control" value="" required="required" pattern="" title="">
					</div>
				</div>
				

				<button type="button" id="guardar" data-operacion="guardaruser" class="btn btn-large btn-block btn-success"  style="margin: 40px 0;">Guardar</button>
			</div>
		</div>
		<div class="row" style="padding-top: 80px;">
			<div class="col-lg-12" style="background-color: #B2EBC5;">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Genero</th>
							<th>acciones</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$count=0;
							if(isset($list)){
								foreach ($list as $dato) {
									$count++;
									echo '
									<tr>
										<td>'.$count.'</td>
										<td>'.$dato['nombre'].'</td>
										<td>'.$dato['genero'].'</td>
										<td>
											<a id="borrar-elemento" data-id="'.$dato['idUsuario'].'" data-user="'.$dato['nombre'].'" href="javascript:void(0)"><i class="glyphicon glyphicon-remove"></i></a><span style="padding-left:10px;"></span>
											<a id="get-elemento" data-id="'.$dato['idUsuario'].'" href="javascript:void(0)"><i class="glyphicon glyphicon-pencil"></i></a>
										</td>
									</tr>
									';
								}
							}else{
								echo '<h1>No existen elementos en la base de datos</h1>';
							}
						?>						
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<br>
				<br>
				<br>
				Mas informacion sobre medoo en <a target="_blank" href="http://medoo.in/doc">documentacion</a>
			</div>
		</div>
	</div>
	
</body>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" scr="js/bootstrap.js"></script>
	<script>
		/*agregar datos nuevos y edita datos existentes*/
		$(document).on('click','#guardar',function(){
			var operacion = $(this).attr('data-operacion');
			var nombre = $('#nombre').val();
			var genero = $('#genero').val();
			/*si agrega nuevo se usa este ajax*/
			if(operacion=='guardaruser'){
				var data = {
					"operaciones": operacion,
					"datos":{
						"nombre" : nombre,
	                	"genero" : genero
					}                
	        	};
	        	$.ajax({
					type: "POST",
					url: 'controller/controller.php',
					data: data,
					dataType: 'html',
					success: function(datasuccess){
						alerta(1);
						$('#nombre').val('');
						$('#genero').val('');
						location.reload();
					}
				});				
			}
			/*si edita se usa este ajax*/
			if(operacion=='editaruser'){
				var id = $(this).attr('data-id');
				var data = {
					"operaciones": operacion,
					"datos":{
						"id":id,
						"nombre" : nombre,
	                	"genero" : genero
					}                
	        	};
	        	$.ajax({
					type: "POST",
					url: 'controller/controller.php',
					data: data,
					dataType: 'html',
					success: function(datasuccess){
						alerta(2);
						$('#nombre').val('');
						$('#genero').val('');
						location.reload();
					}
				});		
			}
		});

		/*obtener datos de tabla */
		$(document).on('click','#get-elemento',function(){
			var id = $(this).attr('data-id');
			var data = {
				"operaciones":'getuser',
				"datos":{
					"id":id
				}
			};
			$.ajax({
				type:"POST",
				url:'controller/controller.php',
				data:data,
				dataType:'html',
				success:function(datasuccess){
				  	$('#guardar').attr('data-operacion','editaruser');
				  	var datos_json = JSON.parse(datasuccess);
				  	$('#guardar').attr('data-id',datos_json.idUsuario);
					$('#nombre').val(datos_json.nombre);
				  	$('#genero').val(datos_json.genero);
				}
			});
		});

		/*eliminar datos de la tabla*/
		$(document).on('click','#borrar-elemento',function(){
			var id = $(this).attr('data-id');
			var user = $(this).attr('data-user');
			var data = {
				"operaciones":'eliminaruser',
				"datos":{
					"id":id
				}
			};
			if (confirm("Desea borrar a "+user) == true) {
				$.ajax({
					type: "POST",
					url: 'controller/controller.php',
					data: data,
					dataType: 'html',
					success:function(datasuccess){
						//console.log(datasuccess);
						alerta(3);
					  	$('#nombre').val('');
					  	$('#genero').val('');
					  	location.reload();
					}
				});
		    } else {
		        
		    }
		});

		/*alerta de eventos*/
		function alerta(tipo){
			/*
			1 = add success
			2 = edit success
			3 = delete sucess
			*/
			if(tipo==1){
				alert('datos agregados exitosamente');
			}
			else if(tipo==2){
				alert('datos modificados exitosamente');
			}
			else if(tipo==3){
				alert('datos eliminados exitosamente');
			}
		}
	</script>
</html>
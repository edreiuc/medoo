<?php 
	/**
	* 
	*/
	require 'conexion.php';
	

	class Usuario
	{	

		function __construct()
		{	

		}

		function getUsuario($data_json){
			$database = new conexion();
			$db=$database->con();
			$data = $db->get("usuario", "*", [
				"idUsuario" => $data_json->id
			]);
			return $data;
		}

		function listarUsuario(){
			$database = new conexion();
			$db=$database->con();
			$data = $db->select("usuario", "*");
			return $data;
		}

		function insertarUsuario($data_json){
			$database = new conexion();
			$db=$database->con();
			$data = $db->insert("usuario",[
				"nombre" => $data_json->nombre,
				"genero" => $data_json->genero
			]);
			return $data;
		}

		function modificarUsuario($data_json){
			$database = new conexion();
			$db=$database->con();
			$data = $db->update("usuario", [
				"nombre" => $data_json->nombre,
				"genero" => $data_json->genero
			], [
				"idUsuario" => $data_json->id
			]);
			return $data;
		}

		function borrarUsuario($data_json){
			$database = new conexion();
			$db=$database->con();
			$data =$db->delete("usuario", [
				"idUsuario" => $data_json->id
			]);
			return $data;
		}


	}
?>